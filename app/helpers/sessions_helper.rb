module SessionsHelper
  def login(user)
    session[:user_id] = user.id
  end

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def login?
    !current_user.nil?
  end

  def set_data_for_react
    gon.logged = login?
    gon.user_type = current_user&.type
    gon.push(current_user: current_user&.cover_up_password)
    gon.csrfToken = form_authenticity_token
    if login?
      gon.user_type = current_user.type
      gon.notify_list = current_user.notifies.created_time_order
    end
  end

  def authenticate_user!
    return if login?
    redirect_to new_sessions_path, flash: { danger: 'ログインしてください' }
  end
end
