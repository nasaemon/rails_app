import React from 'react'
import ReactDOM from 'react-dom'
import SignUpForm from './component/sign_up_form'
class SignUpPage extends React.Component {
  render(){
    return (
      <SignUpForm/>
    )
  }
}

ReactDOM.render(
  <SignUpPage/>, document.querySelector('.sign_up_form')
)
