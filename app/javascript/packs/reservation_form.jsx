import React from 'react'
import ReactDOM from 'react-dom'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import DatePicker from 'material-ui/DatePicker'
import TimePicker from 'material-ui/TimePicker'
import Dialog from 'material-ui/Dialog'
import styled from 'styled-components'
import { MuiThemeProvider } from 'material-ui/styles'
import CsrfToken from './component/csrf_token'

const MainStyle =styled.div`
  text-align: center;
  padding 30px 20px;
`

export default class ReservationForm extends React.Component{
  state = {
    fp_select: gon.reservation.financial_planner_id - 0 || null,
    date: this.to_convert_date(gon.reservation.consultation_start_time),
    time: this.to_convert_date(gon.reservation.consultation_start_time),
    consultation_start_time: null
  }

  handleChange = (event,index,value) => this.setState({fp_select: value})
  onChangeDate = (event,date) => this.setState({date: date})
  onChangeTime = (event,date) => this.setState({time: date})
  onSubmit = () => { this.date_format() }

  date_format = () => {
    const year = this.state.date.getFullYear()
    const month = this.state.date.getMonth()
    const day = this.state.date.getDate()
    const hour = this.state.time.getHours()
    const minutes = this.state.time.getMinutes()

    const date = new Date(year,month,day,hour,minutes)
    this.setState({consultation_start_time: date})
  }

  to_convert_date(date){
    if(date === null){ return date }
    return(new Date(date))
  }

  render(){
    return(
      <MainStyle>
        <MuiThemeProvider>
          <form  action='/reservations' method='post'>
            <CsrfToken/>

            <SelectField
              hintText="申請先FP"
              value={this.state.fp_select}
              onChange={this.handleChange}
            >
              {gon.fp_list.map((fp) =>
                  <MenuItem value={fp.fp_id} primaryText={fp.fp_name} key={fp.fp_name + fp.fp_id}/>
               )}
            </SelectField>
            <input type='hidden' name='reservation[financial_planner_id]' value={this.state.fp_select}/>
            <DatePicker
              hintText="日付選択"
              autoOk={true}
              onChange={this.onChangeDate}
              value={this.state.date}
            />

            <br />
            <TimePicker
              hintText='時間選択'
              minutesStep={30}
              onChange={this.onChangeTime}
              autoOk={true}
              value={this.state.time}
            />
            <input type='hidden' name='reservation[consultation_start_time]' value={this.state.consultation_start_time}/>
           <RaisedButton  label='予約' type='submit' onClick={this.onSubmit} />
          </form>
        </MuiThemeProvider>
      </MainStyle>
    )
  }
}

ReactDOM.render(
  <ReservationForm/>,
  document.querySelector('.reservation_form')
)
