import React from 'react'
import ReactDOM from 'react-dom'
import SignUpForm from './component/sign_up_form'
import RaisedButton from 'material-ui/RaisedButton'
import { MuiThemeProvider } from 'material-ui/styles'
import styled from 'styled-components'

const MainStyle = styled.div`
  padding: 20px;
  text-align: center;
`

const LoginButton = {
  marginLeft: 30,
  color: 'orange',
  marginTop: 10,
  align: 'center',
  width: 300,
}
export default class WelcomePage extends React.Component {
  render() {
    return(
      <MainStyle>
        <MuiThemeProvider>
          <h1>予約アプリへようこそ</h1>
          <p>早速登録してみましょう！</p>
          <SignUpForm/>
          <p>または</p>
          <RaisedButton label='ログイン' primary={true} style={LoginButton} href={Routes.new_sessions_path()} />
        </MuiThemeProvider>
      </MainStyle>
    )
  }
}

ReactDOM.render(
  <WelcomePage/>,
  document.querySelector('.welcome_page')
)
