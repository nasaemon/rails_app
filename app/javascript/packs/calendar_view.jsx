import React from 'react'
import ReactDOM from 'react-dom'
import Calendar from './component/calendar'
import styled from 'styled-components'

const MainStyle = styled.div`
  padding: 20px;
`

export default class CalendarView extends React.Component {
  render(){
    return (
      <MainStyle>
        <Calendar/>
      </MainStyle>
    )
  }
}

ReactDOM.render(
  <CalendarView/>,
  document.querySelector('.calendar')
)
