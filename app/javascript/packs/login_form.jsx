import React from 'react'
import ReactDOM from 'react-dom'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import styled from 'styled-components'
import { MuiThemeProvider } from 'material-ui/styles'
import CsrfToken from './component/csrf_token'

const style = {
  marginLeft: 30,
  marginTop: 10,
  align: 'center',
  width: 300,
}

const MainStyle =styled.div`
  text-align: center;
  padding 30px 20px;
`

export default class LoginForm extends React.Component{
  constructor(props){
    super(props)
    this.state = { user_type: 'GeneralUser' }
  }

  handleChange = (event,index,value) => this.setState({user_type: value})

  render(){
    return(
      <MainStyle>
        <MuiThemeProvider>
          <h1>ログインフォーム</h1>
          <form  action='/sessions' method='post'>
            <input type='hidden' name='_method' value='post' />
            <input type='hidden' name='utf8' value='✓' />
            <input type='hidden' name='authenticity_token' value={gon.csrfToken} />

            <TextField
              hintText= 'your email'
              style={style}
              name='email' type='email'
            />
            <br/>

            <TextField
              hintText= 'password'
              style={style}
              type='password' name='password'
            />

            <br/>
            <RaisedButton style={style} label='ログイン' type='submit' />
            <br/>
          </form>
        </MuiThemeProvider>
      </MainStyle>
    )
  }
}
ReactDOM.render(
  <LoginForm/>, document.querySelector('.login_form')
)
