import React from 'react'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import SwitchReservationStateButton from './switch_reservation_state_button'
import DeleteButton from './delete_button'

export default class FPDialog extends React.Component {
  dialogActions(){
    const actions = [
      <FlatButton
        label="閉じる"
        primary={true}
        onClick={this.props.onClose}
      />
    ]

    if( gon.current_user.id === this.props.selectingEvent.financial_planner.id ){
      actions.push(
        <DeleteButton
          label='削除'
          promary={true}
          href={'/reservations/'+ this.props.selectingEvent.id}
        />
      )
      if( this.props.selectingEvent.state === 'approved' ){
        actions.push(
          <SwitchReservationStateButton
            label='承認キャンセル'
            href={'/reservations/'+ this.props.selectingEvent.id + '/cancel'}
          />
        )
      }else{
        actions.push(
          <SwitchReservationStateButton
            label='承認する'
            href={'/reservations/'+this.props.selectingEvent.id+'/approve'}
          />
        )
      }
    }
    return( actions )
  }

  render(){
    return(
      <Dialog
        title={this.props.selectingEvent.title}
        actions={this.dialogActions()}
        modal={false}
        open={this.props.openDialog}
        onRequestClose={this.props.onClose}
      >
        { this.props.selectingEvent.general_user.name }からの相談申請が来ています。
        承認しますか?
      </Dialog>
    )
  }
}
