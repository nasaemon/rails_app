import React from 'react'
import ReactDOM from 'react-dom'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import CsrfToken from './csrf_token'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

const style = {
  marginLeft: 30,
  marginTop: 10,
  align: 'center',
  width: 300,
}

export default class SignUpForm extends React.Component{
  constructor(props){
    super(props)
    this.state = { user_type: null}
  }

  handleChange = (event,index,value) => this.setState({user_type: value})

  render(){
    return(
      <MuiThemeProvider>
        <form  action='/users' method='post'>
          <CsrfToken/>
          <TextField hintText= 'your name'
            defaultValue={gon.user ? gon.user.name : null }
            style={style}
            name='user[name]' type='text'
          />
          <br/>

          <TextField hintText= 'your email'
            defaultValue={gon.user ? gon.user.email : null }
            style={style}
            name='user[email]' type='email'
          />
          <br/>

          <SelectField
            floatingLabelText="ユーザータイプ"
            value={this.state.user_type}
            disabled={false} style={style}
            onChange={this.handleChange}
          >
            <MenuItem value={'FinancialPlanner'} primaryText="ファイナンシャルプランナー" />
            <MenuItem value={'GeneralUser'} primaryText="一般ユーザー" />
          </SelectField>
          <input type='hidden' name='user[type]' value={this.state.user_type}/>

          <br/>

          <TextField hintText= 'password' style={style} type='password' name='user[password]' />

          <br/>
          <TextField hintText= 'password confirmation' style={style} type='password' name='user[password_confirmation]' />
          <br/>
          <RaisedButton style={style} label='ユーザー登録' secondary={true} type='submit' />
          <br/>
        </form>
      </MuiThemeProvider>
    )
  }
}
