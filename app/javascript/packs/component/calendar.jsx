import React from 'react'
import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import { MuiThemeProvider } from 'material-ui/styles'
import DeleteButton from './delete_button'
import FPDialog from './fp_dialog'
import GUDialog from './gu_dialog'

BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment))

export default class Calendar extends React.Component {
  state = {
    openDialog: false,
    selectingEvent: false
  }

  purseReservationList(){
    const reservation_list = []
    for(const i in gon.reservation_list) {
      const reservation = gon.reservation_list[i]

      reservation_list.push({
        id: reservation.id,
        start: new Date(reservation.start),
        end: new Date(reservation.end),
        title: '開始時刻:' + new Date(reservation.start).toLocaleTimeString(),
        state: reservation.state,
        general_user: reservation.general_user,
        financial_planner: reservation.financial_planner
      })
    }
    return(reservation_list)
  }

  Dialog(){
    if(gon.current_user.type === 'FinancialPlanner'){
      return(
        <FPDialog
          selectingEvent={this.state.selectingEvent}
          openDialog={this.state.openDialog}
          onClose={this.onClose}
          general_user={this.state.selectingEvent.general_user}
          financial_planner={this.state.selectingEvent.financial_planner}
        />
      )
    }else{
      return(
        <GUDialog
          selectingEvent={this.state.selectingEvent}
          openDialog={this.state.openDialog}
          onClose={this.onClose}
          general_user={this.state.selectingEvent.general_user}
          financial_planner={this.state.selectingEvent.financial_planner}
        />
      )
    }
  }

  eventStyle(event) {
    const style = {}
    if(event.state === 'approved'){ style.backgroundColor = 'green' }
    return({ style: style })
  }

  onSelectEvent(event) {
    this.setState({selectingEvent: event})
    this.setState({openDialog: true})
  }

  onClose = () => this.setState({openDialog: false})

  render(){
    return(
      <MuiThemeProvider>
        <BigCalendar
          views={['month', 'day', 'week']}
          events={ this.purseReservationList() }
          defaultView="month"
          defaultDate={new Date(Date.now())}
          eventPropGetter={ (event) => this.eventStyle(event) }
          onSelectEvent={ (event) => this.onSelectEvent(event) }
        />
        {this.state.openDialog ? this.Dialog() : null }
      </MuiThemeProvider>
    )
  }
}
