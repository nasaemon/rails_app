import React, {Component} from 'react'
import PropTypes from 'prop-types'

export default class CsrfToken extends Component  {
  render(){
    return(
      <div>
        <input type='hidden' name='utf8' value='✓' />
        <input type='hidden' name='authenticity_token' value={gon.csrfToken} />
      </div>
    )
  }
}
