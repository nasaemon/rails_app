import React from 'react'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import DeleteButton from './delete_button'
import BigCalendar from 'react-big-calendar'

export default class GUDialog extends React.Component {
  dialogActions(){
    const actions = [
      <FlatButton
        label="閉じる"
        primary={true}
        onClick={this.props.onClose}
      />
    ]

    if( gon.current_user.id === this.props.selectingEvent.general_user.id ) {
      actions.push(
        <DeleteButton
          label='削除'
          promary={true}
          href={'/reservations/'+ this.props.selectingEvent.id}
        />
      )
    }
    return( actions )
  }

  render(){
    return(
      <Dialog
        title={this.props.selectingEvent.title}
        actions={this.dialogActions()}
        modal={false}
        open={this.props.openDialog}
        onRequestClose={this.props.onClose}
      >
        { this.props.selectingEvent.financial_planner.name }への
        申請完了しています。
        あとは待つのみ,,,
      </Dialog>
    )
  }
}
