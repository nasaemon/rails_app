import React from 'react'
import FlatButton from 'material-ui/FlatButton'

export default class SwitchReservationStateButton extends React.Component {
  render(){
    return(
      <FlatButton
        label={ this.props.label }
        primary={true}
        data-method='post'
        href={this.props.href}
      />
    )
  }
}
