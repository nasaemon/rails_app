import React from 'react'
import FlatButton from 'material-ui/FlatButton'

export default class DeleteButton extends React.Component {
  render(){
    return(
      <FlatButton
        label={ this.props.label }
        primary={true}
        data-method='delete'
        href={this.props.href}
      />
    )
  }
}
