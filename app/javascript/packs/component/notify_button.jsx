import React, { Component } from 'react'
import Badge from 'material-ui/Badge'
import NotificationsIcon from 'material-ui/svg-icons/social/notifications'
import IconButton from 'material-ui/IconButton'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import {List, ListItem} from 'material-ui/List'

const NotifyEmpty = () => {
  if(gon.notify_list.length === 0) {
    return(<ListItem primaryText='新しい通知はありません' />)
  }
  else{
    var list = []
    for(var i in gon.notify_list){
      list.push(DecorationItem(gon.notify_list[i]))
    }
    return(list)
  }
}


const DecorationItem = (item) => {
  if(item.decoration === true){
    return(
      <ListItem
        style={ {color: 'red'} }
        primaryText={item.content}
        secondaryText={item.created_at}
      />
    )
  }
  else {
    return(
      <ListItem
        primaryText={item.content}
        secondaryText={item.created_at}
      />
    )
  }
}

export default class NotifyButton extends Component {
  constructor(props){
    super(props)
    this.notify_decoration()
  }

  notify_decoration = () => {
    for(var i =0; i < gon.current_user.notice_counter; i++){
      gon.notify_list[i].decoration = true
    }
  }

  render(){
    return(
      <Badge
        badgeContent={ gon.current_user.notice_counter }
        primary={true}
      >
        <IconMenu
          iconButtonElement={
            <iconButton><NotificationsIcon /></iconButton>
          }
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
        >
          <List>
            <NotifyEmpty/>
          </List>
        </IconMenu>
      </Badge>
    )
  }
}
