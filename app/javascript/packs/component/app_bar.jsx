import React, {Component} from 'react'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import FlatButton from 'material-ui/FlatButton'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import Home from 'material-ui/svg-icons/action/home'
import NotifyButton from './notify_button'

const btnstyle = { color: 'white' }
const navbarColor= { backgroundColor: 'orange' }

const LoginButton = () => (
  <FlatButton style={btnstyle} label='Login' href='/sessions/new' />
)

const set_user_page_url = () => {
  switch(gon.user_type){
    case 'FinancialPlanner':
      return('/financial_planners/'+ gon.current_user.id)
    case 'GeneralUser':
      return('/general_user')
    default:
      return('/')
  }
}


const Logged = () => (
  <div>
    <NotifyButton/>
    <IconMenu
      iconButtonElement={
        <IconButton iconStyle={btnstyle} ><MoreVertIcon/></IconButton>
      }
      targetOrigin={{horizontal: 'right', vertical: 'top'}}
      anchorOrigin={{horizontal: 'right', vertical: 'top'}}
    >
      <MenuItem primaryText="予定確認" href={set_user_page_url()}/>
      <MenuItem primaryText="予約申請" href='/reservations/new' />
      <MenuItem primaryText="Sign out" href='/sessions' data-method='delete' />
    </IconMenu>
  </div>
)

export default class NavBar extends Component {
  render() {
    return (
      <div>
        <AppBar
          title="Rails app"
          style={navbarColor}
          iconElementLeft={<IconButton href='/' ><Home/></IconButton>}
          iconElementRight={ gon.logged ? <Logged/> : <LoginButton/> }
        />
      </div>
    )
  }
}
