import React from 'react'
import ReactDOM from 'react-dom'
import { MuiThemeProvider } from 'material-ui/styles'
import AppBar from './component/app_bar'

const Header = () => (
  <MuiThemeProvider>
    <AppBar/>
  </MuiThemeProvider>
)

ReactDOM.render(
  <Header/>, document.querySelector('#header')
)
