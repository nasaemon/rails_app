class ReservationsController < Reservations::ApplicationController
  before_action :set_reservation, only: %i(destroy)

  def new
    @reservation = Reservation.new
    set_reservation_form_data_for_react
  end

  def index
    @reservations = Reservation.all.consultation_start_time_order
  end

  def create
    @reservation = current_user.reservations.build(reservation_params)
    if @reservation.save
      redirect_to current_user, success: '申請を送りました'
    else
      set_reservation_form_data_for_react
      render 'new'
    end
  end

  def destroy
    if @reservation.destroy
      flash[:success] = '削除しました'
    else
      flash[:danger] = '削除できませんでした'
    end
    redirect_to current_user
  end

  private

  def reservation_params
    params.require(:reservation).permit(:financial_planner_id, :consultation_start_time)
  end

  # @override
  # 親メソッドとは異なるパラメータが必要なので
  def set_reservation
    @reservation = current_user.reservations.find(params[:id])
  end

  def set_reservation_form_data_for_react
    gon.fp_list = FinancialPlanner.all.map { |fp| { fp_name: fp.name, fp_id: fp.id } }
    gon.reservation = @reservation
  end
end
