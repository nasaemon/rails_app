class FinancialPlannersController < UsersController
  def show
    @user = User.find(params[:id])
    set_reservation_list_for_react
  end
end
