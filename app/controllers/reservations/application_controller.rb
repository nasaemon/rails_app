class Reservations::ApplicationController < ApplicationController
  private

  def set_reservation
    @reservation = current_user.reservations.find(params[:reservation_id])
  end
end
