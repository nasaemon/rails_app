class Reservations::CancelsController < Reservations::ApplicationController
  before_action :set_reservation, only: %i(create)

  def create
    @reservation.cancel!
    redirect_to current_user, success: '承認キャンセルしました'
  rescue ActiveRecord::RecordInvalid => e
    redirect_to current_user, danger: e.record.errors.full_messages.join(', ')
  end
end
