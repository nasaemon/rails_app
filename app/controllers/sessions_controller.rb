class SessionsController < ApplicationController
  skip_before_action :authenticate_user!

  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user&.authenticate(params[:password])
      login user
      redirect_to user
    else
      flash.now[:danger] = 'パスワードまたはメールアドレスが違います'
      render 'new'
    end
  end

  def destroy
    session.delete(:user_id)
    @current_user = nil
    redirect_to root_path, success: 'ログアウトしました'
  end
end
