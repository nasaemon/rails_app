class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: %i(new create)
  after_action :reset_notice_counter, only: %i(show)

  def new
    @user = User.new
    gon.user = @user
  end

  def show
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login @user
      redirect_to current_user, success: '登録しました'
    else
      gon.user = @user
      render 'new'
    end
  end

  private

  def user_params
    params
      .require(:user)
      .permit(:name, :email, :type, :password, :password_confirmation)
  end

  def set_reservation_list_for_react
    reservation_list = []
    @user.reservations.each do |reservation|
      reservation_list.push(
        {
          id: reservation.id,
          state: reservation.state,
          start: reservation.consultation_start_time,
          end: reservation.consultation_start_time + 30.minutes,
          general_user: reservation.general_user,
          financial_planner: reservation.financial_planner
        }
      )
      gon.reservation_list = reservation_list
    end
  end

  def reset_notice_counter
    current_user.update!(notice_counter: 0)
  end
end
