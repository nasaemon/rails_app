class GeneralUsersController < UsersController
  def show
    @user = current_user
    set_reservation_list_for_react
  end
end
