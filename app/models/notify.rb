class Notify < ApplicationRecord
  belongs_to :user

  after_create :notice_count_incrment!

  scope :created_time_order, -> { order(created_at: :desc) }

  validates :user, presence: true
  validates :content, presence: true, length: { maximum: 255 }

  private

  def notice_count_incrment!
    user.increment!(:notice_counter)
  end
end
