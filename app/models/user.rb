class User < ApplicationRecord
  has_many :notifies

  has_secure_password
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, uniqueness: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }
  validates :type, presence: true
  validates :password, presence: true, length: { minimum: 6, maximum: 50 }, allow_nil: true

  def cover_up_password
    hash = attributes
    hash.delete('password_digest')
    hash
  end
end
