class Schedule < ApplicationRecord
  belongs_to :financial_planner
  belongs_to :reservation

  validates :financial_planner, presence: true
  validates :reservation, presence: true
  validates :consultation_start_time, presence: true
  validates :financial_planner, uniqueness: { message: '予定が重複しています', scope: %i(consultation_start_time) }

  before_validation :set_financial_planer_by_reservation
  before_validation :set_consultation_start_time_by_reservation

  private

  def set_financial_planer_by_reservation
    self.financial_planner = reservation&.financial_planner
  end

  def set_consultation_start_time_by_reservation
    self.consultation_start_time = reservation&.consultation_start_time
  end
end
