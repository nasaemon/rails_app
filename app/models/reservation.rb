class Reservation < ApplicationRecord
  belongs_to :financial_planner
  belongs_to :general_user
  has_one :schedule

  before_destroy :destroy_schedule
  after_create :make_new_reservation_notify!

  enum state: { unapproved: 0, approved: 1 }

  scope :consultation_start_time_order, -> { order(:consultation_start_time) }

  validates :general_user, presence: true
  validates :financial_planner, presence: true
  validates :consultation_start_time, presence: true

  validate :validate_past, :validate_time_frame, :validate_thirty_minutes_step, if: -> { !!consultation_start_time }
  validate :validate_free_time, if: -> { !!consultation_start_time && !!financial_planner }, on: :create
  validate :validate_duplicate_request, if: -> { !!consultation_start_time && !!general_user }, on: :create

  def validate_past
    errors.add(:base, '過去には遡れませんよ？') if consultation_start_time < Time.zone.now
  end

  def validate_time_frame
    time = consultation_start_time

    case time.wday
    when 0
      errors.add(:base, '日曜は休業日です')
    when 6
      if time.hour < 11 || time.hour > 14
        errors.add(:base, '土曜日の予約枠は 11:00〜15:00です')
      end
    else
      if time.hour < 10 || time.hour > 17
        errors.add(:base, '平日の予約枠は 10:00〜18:00 までです')
      end
    end
  end

  def validate_free_time
    reservations =
      financial_planner.reservations
      .where(consultation_start_time: consultation_start_time)
      .approved

    errors.add(:base, 'FPはその時間に既に予定があります') if reservations.any?
  end

  def validate_duplicate_request
    reservations =
      general_user.reservations.where(consultation_start_time: consultation_start_time)

    errors.add(:base, '同じ時間に申請を出しています') if reservations.any?
  end

  def validate_thirty_minutes_step
    if consultation_start_time.min != 30 && consultation_start_time.min != 0
      errors.add(:base, '30分刻みです')
    end
  end

  def approve!
    transaction do
      create_schedule!
      update!(state: :approved)
      make_approve_notify!
    end
  end

  def cancel!
    transaction do
      schedule.destroy!
      update!(state: :unapproved)
      make_cancel_notify!
    end
  end

  def destroy_schedule
    schedule&.destroy
  end

  def make_new_reservation_notify!
    financial_planner.notifies.create!(content: "#{general_user.name}から予約申請が届きました")
  end

  def make_approve_notify!
    general_user.notifies.create!(content: "#{financial_planner.name}があなたの申請を承認しました")
  end

  def make_cancel_notify!
    general_user.notifies.create!(content: "#{financial_planner.name}が承認キャンセルしました(泣)")
  end

  def purse_status
    approved? ? '承認済み' : '承認待ち'
  end
end
