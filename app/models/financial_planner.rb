class FinancialPlanner < User
  has_many :reservations
  has_many :schedules
end
