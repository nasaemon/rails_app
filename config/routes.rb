Rails.application.routes.draw do
  root 'static_page#home'
  resources :users, only: %i(new create)
  resources :financial_planners, only: %i(show)
  resources :reservations, only: %i(new index create update destroy) do
    resource :approve, only: %i(create), module: :reservations
    resource :cancel, only: %i(create), module: :reservations
  end

  resource :general_user, only: %i(show)
  resource :sessions, only: %i(new create destroy)
end
