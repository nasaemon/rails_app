require 'rails_helper'
include SessionsHelper

describe SessionsController, type: :controller do
  describe 'GET #new' do
    subject(:response) { get :new }

    it { expect(response.status).to eq 200 }
    it { expect(response).to render_template :new }
  end

  let(:user) { create(:general_user) }

  describe 'POST #create' do
    let(:params) { { name: user.name, email: user.email, password: user.password, type: user.type } }

    subject(:response) { post :create, params: params }
    context 'login success' do
      subject { post :create, params: params }

      it { expect(subject.status).to eq 302 }
      it { expect(subject).to redirect_to user }
      it do
        subject
        expect(login?).to be true
      end
    end

    context 'login failed' do
      before { params[:password] = nil }

      it { expect(subject.status).to eq 200 }
      it { expect(subject).to render_template :new }
      it do
        subject
        expect(login?).to be false
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete :destroy, params: { id: user.id } }
    before { login user }

    it { expect(subject.status).to eq 302 }
    it do
      subject
      expect(login?).to be false
    end
  end
end
