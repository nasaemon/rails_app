require 'rails_helper'

describe UsersController, type: :controller do
  describe 'GET #new' do
    before { get :new }

    it { expect(response.status).to eq 200 }
    it { expect(assigns(:user)).to be_a_new(User) }
    it { expect(response).to render_template :new }
  end

  describe 'POST create' do
    subject { post :create, params: params }
    context 'when params is valid data' do
      let(:params) do
        {
          user: {
            name: 'test',
            email: 'testmail@gmail.com',
            type: 'GeneralUser',
            password: 'password'
          }
        }
      end

      it { expect(subject.status).to eq 302 }
      it { expect(subject).to redirect_to(User.last) }
      it { expect { subject }.to change(User, :count).by(1) }
    end

    context 'when params is not valid date' do
      let(:params) do
        {
          user: {
            name: nil,
            type: nil
          }
        }
      end

      it { expect(subject.status).to eq 200 }
      it { expect(subject).to render_template :new }
      it { expect { subject }.not_to change(User, :count) }
    end
  end
end
