require 'rails_helper'
include SessionsHelper

describe ReservationsController, type: :controller do
  let(:user) { create(:general_user) }
  let(:fp) { create(:financial_planner) }

  describe 'GET #new' do
    subject { get :new }

    context 'when user logged in' do
      before { login user }

      it { expect(subject.status).to eq 200 }
      it { expect(subject).to render_template :new }
      it 'is set new reservation' do
        subject
        expect(assigns(:reservation)).to be_a_new(Reservation)
      end
    end

    context 'when user not logged in' do
      before { get :new }
      it_behaves_like 'redirect_to_login_page_if_not_login'
    end
  end

  describe 'GET #index' do
    let(:reservations) { Reservation.all.consultation_start_time_order }

    context 'when user loggd in' do
      before do
        login user
        get :index
      end

      it { expect(response.status).to eq 200 }
      it { expect(response).to render_template :index }

      context 'reservations is nothing' do
        it { expect(assigns(:reservations)).to be_empty }
      end

      context 'reservation is registered' do
        before { create(:reservation) }
        it { expect(assigns(:reservations)).to match_array reservations }
      end
    end

    context 'when user not logged in' do
      before { get :index }

      it_behaves_like 'redirect_to_login_page_if_not_login'
    end
  end

  describe 'POST #create' do
    subject { post :create, params: params }
    before { login user }

    context 'create success' do
      let(:params) do
        {
          reservation: {
            consultation_start_time: Time.zone.local(2040, 12, 5, 15, 30, 0o0),
            financial_planner_id: fp.id
          }
        }
      end
      it { expect(subject.status).to eq 302 }
      it { expect { subject }.to change(Reservation, :count) }
      it { expect(subject).to redirect_to current_user }

      it 'paramsの内容通りに保存できているかテスト' do
        subject
        last_reservation = Reservation.last
        expect(last_reservation.financial_planner_id).to eq params[:reservation][:financial_planner_id]
        expect(last_reservation.consultation_start_time).to eq params[:reservation][:consultation_start_time]
      end
    end

    context "couldn't create reservation" do
      context 'validation error' do
        let(:params) do
          {
            reservation: {
              consultation_start_time: nil,
              financial_planner_id: nil
            }
          }
        end

        it 'do not create reservation' do
          expect {
            subject
          }.not_to change(Reservation, :count)
        end
      end

      context 'invalid_params' do
        it { expect { post :create }.to raise_error(ActionController::ParameterMissing) }
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete :destroy, params: { id: reservation.id } }
    before { login user }

    let!(:reservation) do
      create(
        :reservation,
        financial_planner: fp,
        general_user: user,
        consultation_start_time: Time.zone.local(2043, 12, 3, 15, 30, 0o0)
      )
    end

    it { expect(subject.status).to eq 302 }
    it { expect { subject }.to change(Reservation, :count).by(-1) }
    it { expect(subject).to redirect_to(user) }
  end
end
