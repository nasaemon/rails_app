require 'rails_helper'

describe Notify, type: :model do
  describe 'validation test' do
    it { is_expected.to validate_presence_of(:user) }
    it { is_expected.to validate_presence_of(:content) }
    it { is_expected.to validate_length_of(:content).is_at_most(255) }
  end

  describe 'association' do
    it { is_expected.to belong_to(:user) }
  end

  describe '#notice_count_incrment' do
    let(:user) { create(:general_user) }
    subject { user.notifies.create(content: 'test message') }

    it { is_expected.to callback(:notice_count_incrment!).after(:create) }
    it { expect { subject }.to change { user.notice_counter }.by(1) }
  end
end
