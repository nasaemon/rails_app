require 'rails_helper'

describe Reservation, type: :model do
  let(:reservation) { build(:reservation) }

  it { expect(reservation).to be_valid }

  it 'is set default state unapproved' do
    expect(reservation.state).to eq 'unapproved'
  end

  describe 'association test' do
    it { is_expected.to belong_to(:financial_planner) }
    it { is_expected.to belong_to(:general_user) }
    it { is_expected.to have_one(:schedule) }
  end

  describe 'validation test' do
    it { is_expected.to validate_presence_of(:consultation_start_time) }
    it { is_expected.to validate_presence_of(:general_user) }
    it { is_expected.to validate_presence_of(:financial_planner) }

    it 'is not valid past time' do
      reservation.consultation_start_time = Time.zone.local(1997)
      expect(reservation).not_to be_valid
      expect(reservation.errors[:base]).to include('過去には遡れませんよ？')
    end

    it 'is not valid 30min step' do
      reservation.consultation_start_time = Time.zone.local(2043, 12, 15, 22, 17)
      expect(reservation).not_to be_valid
      expect(reservation.errors[:base]).to include('30分刻みです')
    end

    it 'is not valid sundy' do
      reservation.consultation_start_time = Time.zone.local(2018, 3, 4)
      expect(reservation).not_to be_valid
      expect(reservation.errors[:base]).to include('日曜は休業日です')
    end

    it 'is time frame out' do
      reservation.consultation_start_time = Time.zone.local(2018, 3, 10, 16)
      expect(reservation).not_to be_valid
      expect(reservation.errors[:base]).to include('土曜日の予約枠は 11:00〜15:00です')
    end

    it 'is not valid duplication request' do
      duplicate_reservation = create_duplication_request
      expect(duplicate_reservation).not_to be_valid
      expect(duplicate_reservation.errors[:base]).to include('同じ時間に申請を出しています')
    end

    it 'is not vlaid fp is not free' do
      duplicate_reservation = create_duplication_request
      reservation.approve!
      expect(duplicate_reservation).not_to be_valid
      expect(duplicate_reservation.errors[:base]).to include('FPはその時間に既に予定があります')
    end

    def create_duplication_request
      Reservation.new(
        general_user: reservation.general_user,
        consultation_start_time: reservation.consultation_start_time,
        financial_planner: reservation.financial_planner
      )
    end
  end

  let!(:reservation) { create(:reservation) }

  describe '#approve!' do
    subject { reservation.approve! }

    it 'schedule count increment and reservation.state change to approved' do
      expect { subject }.to change(Schedule, :count).by(1)
        .and change(Notify, :count).by(1)
        .and change { reservation.state }.from('unapproved').to('approved')
    end

    context 'when reservation was already approved' do
      before { reservation.approve! }
      it { expect { subject }.to raise_error(ActiveRecord::RecordNotSaved) }
    end
  end

  describe '#cancel!' do
    subject { reservation.cancel! }
    before { reservation.approve! }

    it 'schedule count decrement and reservation.state change to unapproved' do
      expect { subject }.to change(Schedule, :count).by(-1)
        .and change(Notify, :count).by(1)
        .and change { reservation.state }.from('approved').to('unapproved')
    end
  end

  describe '#destroy' do
    subject { reservation.destroy }
    it { is_expected.to callback(:destroy_schedule).before(:destroy) }

    context 'reservation has schedule' do
      let(:schedule) { reservation.create_schedule }
      it { expect { subject }.to change { schedule.persisted? }.from(true).to(false) }
    end
  end

  describe '#make_new_reservation_notify' do
    subject { create(:reservation) }

    it { is_expected.to callback(:make_new_reservation_notify!).after(:create) }
    it { expect { subject }.to change(Notify, :count).by(1) }
    it { expect(Notify.last.content).to eq "#{reservation.general_user.name}から予約申請が届きました" }
  end
end
