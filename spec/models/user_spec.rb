require 'rails_helper'

describe User, type: :model do
  let(:user) { User.new(params) }
  let(:params) { { name: 'test user', email: 'testemail@gmail.com', type: 'FinancialPlanner', password: 'password' } }

  describe 'validation test' do
    it { expect(user).to be_valid }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_length_of(:email).is_at_most(255) }
    it { is_expected.to validate_presence_of(:type) }
    it { is_expected.to validate_presence_of(:password) }
    it { is_expected.to validate_length_of(:password).is_at_least(6).is_at_most(50) }
    it { is_expected.to have_secure_password }
  end

  describe 'associations' do
    it { is_expected.to have_many(:notifies) }
  end

  it 'initialize notifies counter is zero' do
    expect(user.notice_counter).to be_zero
  end
end
