require 'rails_helper'

describe Schedule, type: :model do
  let(:schedule) { build(:schedule) }

  describe 'callbacks' do
    it { is_expected.to callback(:set_financial_planer_by_reservation).before(:validation) }
    it { is_expected.to callback(:set_consultation_start_time_by_reservation).before(:validation) }
  end

  describe 'validations' do
    it { expect(schedule).to be_valid }

    it { is_expected.to validate_presence_of(:consultation_start_time) }
    it { is_expected.to validate_presence_of(:reservation) }
    it { is_expected.to validate_presence_of(:financial_planner) }

    it 'is not valid because reservation was overlapping' do
      test_schedule = generate_overlapping_schedule
      expect(test_schedule.errors[:financial_planner]).to include '予定が重複しています'
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:financial_planner) }
    it { is_expected.to belong_to(:reservation) }
  end

  describe 'set data when validation' do
    let(:reservation) { create(:reservation) }
    let(:schedule) { reservation.build_schedule }

    describe '#set_financial_planer_by_reservation' do
      subject { schedule.send(:set_financial_planer_by_reservation) }

      it { expect { subject }.to change { schedule.financial_planner }.from(nil).to(reservation.financial_planner) }
    end

    describe '#set_consultation_start_time_by_reservation' do
      subject { schedule.send(:set_consultation_start_time_by_reservation) }

      it { expect { subject }.to change { schedule.consultation_start_time }.from(nil).to(reservation.consultation_start_time) }
    end
  end

  def generate_overlapping_schedule
    fp = create(:financial_planner)
    reservation = fp.reservations.create(
      general_user: create(:general_user),
      consultation_start_time: Time.zone.local(2020, 2, 3, 14)
    )
    same_reservation = fp.reservations.create(
      general_user: create(:general_user),
      consultation_start_time: Time.zone.local(2020, 2, 3, 14)
    )

    reservation.create_schedule
    same_reservation.create_schedule
  end
end
