shared_examples 'redirect_to_login_page_if_not_login' do
  it { expect(response.status).to eq 302 }
  it { expect(response).to redirect_to new_sessions_url }
end
