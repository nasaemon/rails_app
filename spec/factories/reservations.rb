FactoryBot.define do
  factory :reservation do
    consultation_start_time Time.zone.local(2040, 12, 12, 12, 0o0)
    association :financial_planner
    association :general_user
  end
end
