FactoryBot.define do
  factory :financial_planner do
    name 'test user'
    type 'FinancialPlanner'
    sequence(:email) { |n| "test-no#{n}@gmail.com" }
    password 'password'
  end

  factory :general_user do
    name 'general user'
    sequence(:email) { |n| "test-no#{n}@gmail.com" }
    type 'GeneralUser'
    password 'password'
  end
end
