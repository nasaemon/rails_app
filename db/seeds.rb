3.times do |n|
  User.create(
    name:  "fp-#{n}",
    type: 'FinancialPlanner',
    password: 'password',
    password_confirmation: 'password'
  )
end

20.times do |n|
  User.create(
    name:  "gu-#{n}",
    type: 'GeneralUser',
    password: 'password',
    password_confirmation: 'password'
  )
end

User.create(
  name:  "fp",
  type: 'FinancialPlanner',
  password: 'password',
  password_confirmation: 'password'
)

User.create(
  name:  "gu",
  type: 'GeneralUser',
  password: 'password',
  password_confirmation: 'password'
)
