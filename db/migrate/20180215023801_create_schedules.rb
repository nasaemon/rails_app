class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
      t.integer :financial_planner_id, null: false
      t.integer :reservation_id, null: false
      t.datetime :consultation_start_time, null: false
      t.timestamps
    end

    add_index :schedules, %i(financial_planner_id consultation_start_time), unique: true,
      name: :add_unique_index_FP_id_and_consultation_start_time
  end
end
