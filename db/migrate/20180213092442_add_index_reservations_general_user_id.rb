class AddIndexReservationsGeneralUserId < ActiveRecord::Migration[5.1]
  def change
    add_index :reservations, :financial_planner_id
    add_index :reservations, %i(general_user_id consultation_start_time), unique: true, name: :add_unique_index_general_user_id_and_start_time
  end
end
