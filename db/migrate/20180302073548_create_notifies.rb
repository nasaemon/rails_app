class CreateNotifies < ActiveRecord::Migration[5.1]
  def change
    create_table :notifies do |t|
      t.integer :user_id, null: false
      t.string :content, null: false

      t.timestamps
    end

    add_index :notifies, :user_id
  end
end
