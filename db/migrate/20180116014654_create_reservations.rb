class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.integer :general_user_id, null: false
      t.integer :financial_planner_id, null: false
      t.datetime :consultation_start_time, null: false
      t.integer :state, null: false, default: 0

      t.timestamps
    end
  end
end
